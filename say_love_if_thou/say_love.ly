\version "2.24.3"
\language "deutsch"

#(set-global-staff-size 18)

\paper {

}

\header {
    title = "Say Love, If Ever Thou Didst Find"
    composer = "John Dowland"
    tagline = "Notation typesetting by Marco Heins"
}

global = {
  \time 4/4
  \partial 4
  \key g \major
}

verseOne = \lyricmode {
  Say, Love, if ev -- er thou didst find A wo -- man with a con -- stant mind?
  None but one. And what should that rare mir -- ror be? 
  Some god -- dess or some queen is she?
  
  She, she, she, she, she, she and on -- ly
  she, She on -- ly queen of Love and Beau -- ty.
}

verseTwo = \lyricmode {
 But could thy fie -- ry poi -- sond dart At no time __ touch her spot -- less heart
 Nor come near? She is not sub -- ject to Love's bow;
 Her eye com -- mands, her heart saith No;

}

verseThree = \lyricmode {
 How might I that fair won -- der know That mocks de -- sire with end -- less No?
 See the moon That ev -- er in one change doth grow, 
 Yet still the same. And she is so.
}

verseFour = \lyricmode {
  To her then yield thy shafts and bow, That can com -- mand af -- fec -- tions so.
  Love is free, So are her thoughts that van -- quish thee! 
  There is no queen of Love but she;
}

sopran = 
\new Voice = "sopran"
  \relative c'' {
  
  d4
  c h a g
  a a g g
  fis g a d

  d cis d2
  a4 h c2
  r2. r8 h
  a4 g fis g

  a h a c
  h a gis a 
  h c h2
  
  r4 c r a
  r h r g 
  \time 6/4
  g g g g2 fis4
  
  \time 4/4
  g g g g 
  g g g g 
  g2 g
}

alt = 
\new Voice = "alt"
  \relative c'' {

  g4
  g g fis g
  g fis g d
  d  c8 ( h )  a4 a'
  
  a4. g8 fis2
  f4 d c2
  r2. r8 d
  d4 d d d
  
  d d d e
  e e e e
  e e e2
  
  e4 r d r
  d r e r
  c d e d d4. c8
  h4 d e d 
  c h c d
  e2 d
  
}

tenor = 
\new Voice = "tenor"
  \relative c'' {

  h4
  e d d h8 c ~
  c a ( d c ) h4 h
  a e' d fis
  
  e e d2
  r1
  g,4 a h4. g8
  fis4 g a h

  a g fis a
  gis a h c
  h a gis2

  e4 r d r
  g r g r
  e' d c h a2
  g4 h c h 
  e d e d ~
  d8 g,8 ( c4 ) h2
  
}

bass =
\new Voice = "bass"
  \relative c {
  \clef "treble_15"
  %\clef bass
 
  g4
  c g d' e
  d d g, g'
  d e fis d
  
  a' a, d2
  r1
  e4 d g,4. g8
  d'4 g, d' g
  
  fis g d a
  e' a, e' a
  gis a e2
  
  \break
  
  a,4 r d r
  g, r c r
  c h c g d' d
  g,4 g' fis g
  c, g c h
  c2 g
  
}

\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Sopran" }
      << \global \sopran
         \new Lyrics \lyricsto "sopran" { \verseOne }
    >>
    \new Staff \with { instrumentName = "Alt" }
      << \global \alt
         \new Lyrics \lyricsto "alt" { \verseTwo }
    >>
    \new Staff \with { instrumentName = "Tenor" }
      << \global \tenor
         \new Lyrics \lyricsto "tenor" { \verseThree }
    >>
    \new Staff \with { instrumentName = "Bass" }
      << \global \bass
         \new Lyrics \lyricsto "bass" { \verseFour }
    >>
  >>
  \layout {}
  \midi {}
}
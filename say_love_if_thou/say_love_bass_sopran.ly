\version "2.24.3"
\language "deutsch"

#(set-global-staff-size 25)

\paper {

}

\header {
    title = "Say Love, If Ever Thou Didst Find" 
    composer = "John Dowland"
    tagline = "Notation typesetting by Marco Heins"
}

global = {
  \time 4/4
  \partial 4
  \key g \major
  \clef "treble_8"
}

verseOne = \lyricmode {
  Say, Love, if ev -- er thou didst find A wo -- man with a con -- stant mind?
  None but one. And what should that rare mir -- ror be? 
  Some god -- dess or some queen is she?
  
  She, she, she, she, she, she and on -- ly
  she, She on -- ly queen of Love and Beau -- ty.
}

verseTwo = \lyricmode {
 But could thy fie -- ry poi -- sond dart At no time __ touch her spot -- less heart
 Nor come near? She is not sub -- ject to Love's bow;
 Her eye com -- mands, her heart saith No;

}

verseThree = \lyricmode {
 How might I that fair won -- der know That mocks de -- sire with end -- less No?
 See the moon That ev -- er in one change doth grow, 
 Yet still the same. And she is so.
}

verseFour = \lyricmode {
  To her then yield thy shafts and bow, That can com -- mand af -- fec -- tions so.
  Love is free, So are her thoughts that van -- quish thee! 
  There is no queen of Love but she;
}

sopran = 
\new Voice = "sopran"
  \relative c' {
  \voiceOne
  d4
  c h a g
  a a g g
  fis g a d

  d cis d2 
  a4 h c2
  \skip 2. \skip 8 h8
  a4 g fis g

  a h a c \break
  h a gis a 
  h c h2
  
  \skip 4 c4 \skip 4 a
  \skip 4 h \skip 4 g   
  \time 6/4
  g g g g2 fis4
  
  \time 4/4
  g g g g 
  g g g g 
  g2 g
}

alt = 
\new Voice = "alt"
  \relative c' {
  \voiceOne
  
  g4
  g g fis g
  g fis g d
  d  c4  a4 a'
  
  a4 g fis2
  f4 d c2
  \skip 2 \skip 4. d8
  \skip 4 d4 \skip 4 d
  
  d d \skip 4 e
  \skip 4 e e e
  e e e2
  
  e4 \skip 4 d \skip 4
  d \skip 4 e \skip 4
  \skip 4 d e d \skip 4 \skip 4
  h4 d e d 
  \skip 4 h \skip 4 d
  e2 d
  
}

tenor = 
\new Voice = "tenor"
  \relative c' {
  \voiceOne

  h4
  e, d \skip 2 
  \skip 2  h4 h
  a e' \skip 4 fis4
  
  \skip 1
  \skip 2 \skip 4
  g4 a h4. g8
  fis4 g \skip 4 h,

  \skip 4 g' fis a
  gis \skip 4 h, c
  h a' gis2

  e4 \skip 4 \skip 2
  g \skip 4 g \skip 4
  e d \skip 4 h \skip 2
  \skip 4 h c h 
  e d e d ~
  \skip 2  h2
  
}

bass =
\new Voice = "bass"
  \relative c {

  \voiceTwo
 
  g4
  c g d' e
  d d g, g
  \skip 4 e fis d'
  
  a a d2
  \skip 1
  e4 d g,4. g8
  d'4 g, d' g,
  
  fis g d' a
  e' a, e a
  gis a e2
  
  a4 \skip 4 d \skip 4
  g, \skip 4 c \skip 4
  c h c g \skip 4 d'
  g,4 g fis g
  c g c h
  c2 g
  
}

\score {
  \new StaffGroup <<
    \new Staff << \global \sopran \alt \tenor \bass 
    \new Lyrics \lyricsto "sopran" { \verseOne }
    >>
  >>
  \layout {}
  \midi {}
}
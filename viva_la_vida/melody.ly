\version "2.24.3" % Die Version von lilypond, die ich verwende
\include "deutsch.ly" % Die Spracheinstellung für die Lyrics


#(set! paper-alist (cons '("small" . (cons (* 200 mm) (* 100 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 10\mm
}

\header {
    tagline = ##f
}


harmonies = \chordmode {
  \set chordNameLowercaseMinor = ##t
  
  e1:m/g c:/g d:7/fis g:/d e:m/g
}

melody = \transpose a g {
  \relative c'' {
  \clef treble
  \key a \major % Die Tonart festlegen
  \time 4/4 % Die Taktart festlegen
  \tempo 4 = 138 % Das Tempo festlegen

  r4. cis8 cis4 cis
  cis2. d8 h ~
  h2 r8 h4 a8
  h4 h8 a cis4 gis8 a8 ~ a
  }
}

words = \lyricmode {
  I used to rule the world
  Seas would rise when I gave the word...
}

\score {
  <<
    \new ChordNames {
      \set chordChanges = ##t
      \harmonies
    }
  \new Voice = "melody" { \autoBeamOff \melody }
  \new Lyrics \lyricsto "melody" \words
  >>
  \layout { } % Das Layout festlegen
  \midi { } % Die MIDI-Ausgabe ermöglichen
}
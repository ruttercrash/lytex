\version "2.20.0"
\language "deutsch"

#(set! paper-alist (cons '("small" . (cons (* 200 mm) (* 100 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 10\mm
}

\header {
    tagline = ##f
}

harmonies = \chordmode {
  \set chordNameLowercaseMinor = ##t
 
  c:/g d:7/fis g:/d e:m/g
}

c_e =  <c e>\staccato
c_d = <c d>\staccato 
g_d = <g d'>\staccato 
h_e = <h e>\staccato 


violin = \relative c'' {
   \time 4/4
  \key g \major
  
  \c_e \c_e \c_e  <c e>8 \staccato <c d>8 \staccato
  r8 \c_d <c d>8 \staccato \c_d \c_d
  \g_d \g_d \g_d <g d'>8 \staccato <h e>8 \staccato
  r8 \h_e <h e>8 \staccato \h_e \h_e
}

g_c = \relative c' { <g c> \staccato }
fis_c = \relative c { <fis c'> \staccato }

viola = \new Voice \relative c' {
  %\clef alto
  \g_c \g_c \g_c <g c>8 \staccato <fis c'>8 \staccato 
  r8 \fis_c <fis c'>8 \staccato <fis c'>4 \staccato \fis_c
}

\score {
  <<
   \new Staff \with { instrumentName = "Violin" }
     \new Voice { \violin }
  >>
  
  \layout {}
}
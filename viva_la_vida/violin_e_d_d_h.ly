\version "2.20.0"
\language "deutsch"

#(set! paper-alist (cons '("small" . (cons (* 200 mm) (* 100 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 10\mm
}

\header {
    tagline = ##f
}

harmonies = \chordmode {
  \set chordNameLowercaseMinor = ##t
 
  c:/g d:7/fis g:/d e:m/g
}

c_e =  <c e>\staccato
c_d = <c d>\staccato 
g_d = <g d'>\staccato 
h_e = <h e>\staccato 


violin = \relative c'' {
   \time 4/4
  \key g \major
  
  e2.^\markup { molto vibrato  } ~ e8 d8 ~ d1
  d2. ~ d8 h8 ~ h1 
}

g_c = \relative c' { <g c> \staccato }
fis_c = \relative c { <fis c'> \staccato }

viola = \new Voice \relative c' {
  %\clef alto
  \g_c \g_c \g_c <g c>8 \staccato <fis c'>8 \staccato 
  r8 \fis_c <fis c'>8 \staccato <fis c'>4 \staccato \fis_c
}

\score {
  <<
   \new Staff \with { instrumentName = "Violin" }
     \new Voice { \violin }
  >>
  
  \layout {}
}
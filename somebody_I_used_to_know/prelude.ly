\version "2.20.0"
\language "deutsch"

#(set! paper-alist (cons '("small" . (cons (* 150 mm) (* 100 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 10\mm
}

\header {
    tagline = ##f
}

global = {
  \time 4/4

}

alt = { 
  c c d d
  a'8 b c a g2
  f4 f e e
  d d c2 
}

gynna = 
\new Voice 
  \transpose f c
    \relative c'' {
  \key f \major
  \alt \break
  \repeat volta 2 { \alt }
}


marco = 
\new Voice 
  \relative c'' {
  \key c \major
  
  r1 r1 r1 r1
  e1 ~ e2 g4. e8
  d4.a4.a4 ~
  a2 h4. a8
}

\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Gynna" }
      << \global \gynna
    >>
    \new Staff \with { instrumentName = "Marco" }
      << \global \marco
    >>
  >>
  \layout {}
  \midi {}
}
\version "2.20.0"
\language "deutsch"

#(set! paper-alist (cons '("snall" . (cons (* 100 mm) (* 30 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 20\mm
}

\header {
    tagline = ##f
}

global = {
  \time 4/4

}


gynna = 
\new Voice 
  \transpose f c
    \relative c'' {
  \key f \major
      
  c c d d
  a'8 b c a g2
  f4 f e e
  d d c2 
}


marco = 
\new Voice 
  \relative c'' {
  \key c \major
  e1 ~ e2 g4. e8
  d4.a4.a4 ~
  a2 h4. a8
}

\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Gynna" }
      << \global \gynna
    >>
    \new Staff \with { instrumentName = "guitar" }
      << \global \marco
    >>
  >>
  \layout {}
  \midi {}
}
\version "2.20.0"
\language "deutsch"

#(set! paper-alist (cons '("small" . (cons (* 150 mm) (* 30 mm))) paper-alist))

\paper {
  #(set-paper-size "small")
  left-margin = 20\mm
}

\header {
    tagline = ##f
}

global = {
  \time 4/4

}


marco = 
\new Voice 
  \relative c {
  \clef bass
  \key c \major
  
  a4. a8 g4. g8
  a4. a8 g4. r8
  
  a4. a8_\markup { Now and then I think... } g4. g8
  a4. a8 g4. r8
}

\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "bass" }
      << \global \marco
    >>
  >>
  \layout {}
  \midi {}
}
\version "2.24.0"
\language "deutsch"
\header {
    title = "Oblivion"
    composer = "Astor Piazolla"
    tagline = "Notation typesetting by Marco Heins"
}

\paper {

  system-system-spacing =
    #'((basic-distance . 15) 
       (minimum-distance . 8)
       (padding . 1)
       (stretchability . 60)) 
}

violin = \relative c' {
  \time 4/4
  \tempo 4 = 96
  \key c \major

  r1^\markup { "Intro guitar" } r1 r1 r1 \break
  
  \repeat segno 2 {
  
  a8\mp^\markup { "Moderatamente" } e'(f) a h(c) d4
  a,8 e'(f) a c(d) e4
  a,,8 e'(f) a h(c) d4
  a,8 e'(f) a c(d) e4 \break

  d,8 c' a f h, h' g h
  c, h' g e a, c' a c 
  
  h, d' a f h, dis' a dis
  e c h a gis4 h \break
  e'1\mf
  \tuplet 3/2 { e4 d c } h4. a8
  c1 
  
  c4 h8 a g4. f8
  a4 a2.~ 
  a4 g8 f e4. d8 
  f1 \break
  r2 r8 a\< d e
  f1 \f
  
  \tuplet 3/2 { f4 e d } c h
  e1 
  \tuplet 3/2 { e4 d c } h a
  dis,1 \break
  
  \alternative {
    \volta 1 {
  r2 r4 e
  a1
  a2. r4 
  
  c2.\mp a8 h
  \tuplet 3/2 { c4 h d } g, f
  e h'2. ~ \break
  \tuplet 3/2 { h4 a c } f, e

   d4 a'2. ~
    \tuplet 3/2 { a4 gis h } e, d
    c4.\< d e4 ~
    e2. r4 

  c'2.\f a8 h \break
  \tuplet 3/2 { c4 h d } g, f
  e h'2. ~
  \tuplet 3/2 { h4 a c } f, e 

  dis dis2 dis4 
    e2\> e
    a1\p ~ a2. r4 \break
  
    } % volta 1 end
  } % alternative end
    \volta 2 \volta #'() {
      \section
      \sectionLabel "Coda"
    } % volta 2 end
  } % repeat segno end
  \tuplet 3/2 {  r4 a a } a a
  a1\> a a ~ a2 a a1\flageolet\!
  \fine
}

\score {
   \new Staff
     \with { instrumentName = "Violin" }
     \with { midiInstrument = "violin" }
     \new Voice { \violin }
  
  \layout {}
  \midi {}
}